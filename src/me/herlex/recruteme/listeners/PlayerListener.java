package me.herlex.recruteme.listeners;

import java.util.HashMap;
import java.util.Map;
import me.herlex.recruteme.RecruteMe;
import me.herlex.recruteme.commands.CommandRecrute;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerListener implements Listener
{
    private final RecruteMe main;
    private final CommandRecrute recrute;
    Map<String, Long> sessions = new HashMap<String, Long>();
    Player player;
    
    public PlayerListener(RecruteMe main, CommandRecrute recrute)
    {
        this.main = main;
        this.recrute = recrute;
    }
    @EventHandler
    public void onJoin(PlayerJoinEvent event)
    {
        player = event.getPlayer();
        long time = main.getData().getLong("recrutes." + event.getPlayer().getName() + ".time", 0L);
        if(time < main.getConfig().getLong("main.playtime"))
        {
            sessions.put(event.getPlayer().getName(), System.currentTimeMillis());
        }
    }
    
    @EventHandler
    public void onQuit(PlayerQuitEvent event)
    {
        Long session = sessions.get(event.getPlayer().getName());
        if(session == null)
            return;
             long time = main.getData().getLong("recrutes." + event.getPlayer().getName() + ".time", 0L);
        if(time <= main.getConfig().getLong("main.playtime"))
        {
            if(main.getData().getString("recrutes." + event.getPlayer().getName() + ".accept", "").equalsIgnoreCase("true"))
            {
            long played = System.currentTimeMillis() - session.longValue();
            long stored = main.getData().getLong("recrutes." + event.getPlayer().getName()+".time", 0L);
            main.getData().set("recrutes." + event.getPlayer().getName()+".time", stored+played);
            main.save();
            }
        }
    }
    public Map<String,Long> getSessions()
    {
        return this.sessions;
    }
}

