package me.herlex.recruteme;
     
import java.io.File;
import me.herlex.recruteme.listeners.PlayerListener;
import me.herlex.recruteme.commands.RMCE;
import me.herlex.recruteme.commands.CommandRecrute;    
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import net.milkbowl.vault.chat.Chat;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.economy.EconomyResponse;
import net.milkbowl.vault.permission.Permission;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
     
public class RecruteMe extends JavaPlugin
{
    private File file;
    @SuppressWarnings("NonConstantLogger")
    public  Logger logger = Logger.getLogger("Minecraft");
    private PlayerListener pl;
    private YamlConfiguration data;
    public static Economy econ = null;
    public static Permission perms = null;
    public static Chat chat = null;
    private CommandRecrute recrute;
    Map<String, String> recrutes = new HashMap<String, String>();

    @Override
    public void onEnable()
    {
        PluginDescriptionFile pdFile = this.getDescription();
        logger.info(new StringBuilder(pdFile.getName()).append(" Version ").append(pdFile.getVersion()).append(" Has been Enabled!").toString());
        getCommand("rm").setExecutor(new RMCE(this));
        this.recrute = new CommandRecrute(this);
        this.pl = new PlayerListener(this, this.recrute);
        Bukkit.getPluginManager().registerEvents(this.pl, this);
        this.file = new File(getDataFolder(), "recrute.yml");
                
        this.reloadConfig();
        this.getConfig().addDefault("main.playtime", "3600000");
        this.getConfig().addDefault("main.chatcolor", "&b");
        this.getConfig().addDefault("rewards.Items.enabled", "false");
        this.getConfig().addDefault("rewards.Items.items", new String[]{
            "ItemID,Amount"
        });
        this.getConfig().addDefault("rewards.iConomy.enabled", "true");                
        this.getConfig().addDefault("rewards.iConomy.amount", "1000");
        this.getConfig().options().copyDefaults(true);
        this.saveConfig();
                
        if (!setupEconomy() ) {
            logger.severe(String.format("[%s] - Disabled due to no Vault dependency found!", getDescription().getName()));
            getServer().getPluginManager().disablePlugin(this);
            return;
        }
        setupPermissions();
        setupChat();
        
        if(!file.exists())
        {
            try
            {
                file.getParentFile().mkdirs();
                if(!file.createNewFile())
                {
                    throw new IOException("Failed to create new file?");
                }
            }
            catch(IOException ex)
            {
            }
        }
        this.data = YamlConfiguration.loadConfiguration(getFile());
        ConfigurationSection recrutesSection = getData().getConfigurationSection("recrutes");
        if(recrutesSection == null) 
            recrutesSection = getData().createSection("recrutes");
        for(String recrute : recrutesSection.getKeys(false))
        {
            recrutes.put(recrute, recrutesSection.getString(recrute+".recruter", ""));
        }
	

        new BukkitRunnable()
        {
            Player player;
            Player recrutedone;
       
            List<String> marked = new ArrayList<String>();
       
            String other;
            String recruted;
       
            @Override
            public void run()
            {
                for(Map.Entry<String, Long> entry : pl.getSessions().entrySet())
                {
                    if(entry.getValue() == null) continue;
                    long time = getData().getLong("recrutes." + entry.getKey() + ".time", 0L);
                    long played = System.currentTimeMillis() - entry.getValue().longValue();
                    if(time+played >= getConfig().getLong("main.time"))
                    {
                        other = recrutes.get(entry.getKey());
                        recruted = entry.getKey();
                        if(other == null)
                        {
                            marked.add(entry.getKey());
                        }
                        player = Bukkit.getPlayerExact(other);
                        recrutedone = Bukkit.getPlayerExact(recruted);
                        if(player != null)
                        {
                            EconomyResponse recruter;
                            EconomyResponse recruted;
                            
                            if(getConfig().getString("rewards.iConomy.enabled").equalsIgnoreCase("true"))
                            {
                                recruted = econ.depositPlayer(recrutedone.getName(), getConfig().getLong("rewards.iConomy.amount"));
                                recruter = econ.depositPlayer(player.getName(), getConfig().getLong("rewards.iConomy.amount"));                                
                            
                                if(recruted.transactionSuccess() && recruter.transactionSuccess())
                                {
                                    recrutedone.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("main.chatcolor") + "[RecruteMe]: You received your Reward!"));
                                    player.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("main.chatcolor") + "[RecruteMe]: You received your Reward!"));
                                } 
                                else 
                                {
                                    player.sendMessage(ChatColor.RED + "An error occured with RecruteMe! Please contact the ServerOwner!");
                                }
                            }
                            if(getConfig().getString("rewards.Items.enabled").equalsIgnoreCase("true"))
                            {
                                List<String> items = getConfig().getStringList("rewards.Items.items");
                                if(items != null && items.size() > 0)
                                {
                                    for(int j = 0; j < items.size(); j++)
                                    {
                                        String CurrentItem = items.get(j);
                                        String[] CurrentItemSplit = CurrentItem.split(",");
                                        String ItemIDString = CurrentItemSplit[0];
                                        String AmountString = CurrentItemSplit[1];
                                        int ItemID = Integer.parseInt(ItemIDString);
                                        int Amount = Integer.parseInt(AmountString);
                                        ItemStack CurrentItemStack = new ItemStack(ItemID, Amount);
                                        player.getInventory().addItem(CurrentItemStack);
                                        recrutedone.getInventory().addItem(CurrentItemStack);
                                    }
                                }
                                recrutedone.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("main.chatcolor") + "[RecruteMe]: You received your Reward!"));
                                player.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("main.chatcolor") + "[RecruteMe]: You received your Reward!"));
                            }
                            getData().set("recrutes." + entry.getKey() + ".time", getConfig().getLong("main.playtime") + 1);
                        }
                        marked.add(entry.getKey());
                    }
                }
                for(String player : marked)
                {
                    pl.getSessions().remove(player);
                }
                marked.clear();
            }
        }.runTaskTimer(this, 30L, 200L);
    }
           
    @Override
    public void onDisable()
    {
        PluginDescriptionFile pdfFile = this.getDescription();
        Bukkit.getScheduler().cancelTasks(this);
        logger.info(new StringBuilder(pdfFile.getName()).append(" Has been Disabled!").toString());
    }
           
    /**
     *
     * @return
     */
    @Override
    public File getFile()
    {
        return this.file;
    }
           
    public YamlConfiguration getData()
    {
        return this.data;
    }
           
    public void save()
    {
        try
        {
            this.data.save(getFile());
        }
        catch(IOException ex)
        {
        }
    }
    private boolean setupEconomy() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        econ = rsp.getProvider();
        return econ != null;
    }

    private boolean setupChat() {
        RegisteredServiceProvider<Chat> rsp = getServer().getServicesManager().getRegistration(Chat.class);
        chat = rsp.getProvider();
        return chat != null;
    }

    private boolean setupPermissions() {
        RegisteredServiceProvider<Permission> rsp = getServer().getServicesManager().getRegistration(Permission.class);
        perms = rsp.getProvider();
        return perms != null;
    }
}

