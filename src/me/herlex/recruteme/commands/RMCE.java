package me.herlex.recruteme.commands;

import java.util.ArrayList;
import java.util.Arrays;
import me.herlex.recruteme.RecruteMe;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class RMCE implements CommandExecutor
{
    protected final CommandHelp help;
    protected final CommandRecrute recrute;
    //protected final CommandList list;
    protected final CommandAccept accept;
    
    
    public RMCE(RecruteMe main)
    {
        help = new CommandHelp(main);
        recrute = new CommandRecrute(main);
        //list = new CommandList(main);
        accept = new CommandAccept(main);
    }
    
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
    {		

        Player player = (Player) sender;

        if(args.length < 1)
        {
            args = new String[]{"help"};
        }

        if (commandLabel.equalsIgnoreCase("rm")) 
        {
            String command = args[0];
            args = Arrays.copyOfRange(args, 1, args.length);
            
            if(command.equalsIgnoreCase("recrute"))
            {
                if(args.length <1 || args.length >1)
                {
                    player.sendMessage("Correct Usage: /rm recrute <name>");
                }
                else
                {
                return recrute.execute(player, args);
                }
            }
            if(command.equalsIgnoreCase("accept"))
            {
                if(args.length <1 || args.length >1)
                {
                    player.sendMessage("Correct Usage: /rm accept <name>");
                }
                else
                {
                return accept.execute(player, args);
                }
            }
            /**if(command.equalsIgnoreCase("list"))
            * {
            *     return list.execute(player, args);
            * }
            */
            if(command.equalsIgnoreCase("help"))
            {
                ArrayList<String> arguments = new ArrayList<String>(Arrays.asList(args));
                arguments.add(0, command);

                return help.execute(player, arguments.toArray(new String[0]));
            }

        }
        return false;
    }

}
