package me.herlex.recruteme.commands;
     
import me.herlex.recruteme.RecruteMe;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
 

public class CommandAccept extends CommandBase
{
   public CommandAccept (RecruteMe main)
   {
       super(main);
   }
       
   public boolean execute(Player player, String[] args)
   {
       Player other = Bukkit.getPlayerExact(args[0]);
       if(player.hasPermission("recruteme.accept") || player.hasPermission("recruteme.*")) 
       {
            if(other == null)
            {
                player.sendMessage(ChatColor.translateAlternateColorCodes('&', main.getConfig().getString("main.chatcolor") + args[0] + " is offline! Try it again later!"));
                return true;
            }
            if(main.getData().getString("recrutes." + args[0] + ".recruter", "").equalsIgnoreCase(player.getName()))
            {
                if(main.getData().getString("recrutes." + args[0] + ".accept", "").equalsIgnoreCase("false"))
                {
                    main.getData().set("recrutes." + args[0] + ".accept", "true");
                    player.sendMessage(ChatColor.translateAlternateColorCodes('&', main.getConfig().getString("main.chatcolor") + "You accepted the Recruitment of " + args[0] + " !"));
                    other.sendMessage(ChatColor.translateAlternateColorCodes('&', main.getConfig().getString("main.chatcolor") + player.getName() + " accepted the Recruitment!"));
                }
                else
                {
                    player.sendMessage(ChatColor.translateAlternateColorCodes('&', main.getConfig().getString("main.chatcolor") + "You already accepted the Recrute!"));
                }
            }
            else
            {
                player.sendMessage(ChatColor.translateAlternateColorCodes('&', main.getConfig().getString("main.chatcolor") + args[0] + " must first type /rm recrute " + player.getName()));
            }
       }
       else
       {
           player.sendMessage(ChatColor.RED + "You don't have the Permissions!");
       }
       return true;
   }
}


