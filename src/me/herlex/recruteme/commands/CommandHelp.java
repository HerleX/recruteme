package me.herlex.recruteme.commands;

import me.herlex.recruteme.RecruteMe;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class CommandHelp extends CommandBase
{
    
    protected CommandHelp(RecruteMe main)
    {
        super(main);
    }
    
    public boolean execute(Player player, String[] args)
    {
        player.sendMessage(ChatColor.AQUA + "############RecruteMe############");
        player.sendMessage(ChatColor.GRAY + "/rm  help" + ChatColor.WHITE + "- Opens this Help");
        player.sendMessage(ChatColor.GRAY + "/rm  recrute <name>" + ChatColor.WHITE + "- For Recruted Players");   
        player.sendMessage(ChatColor.GRAY + "/rm  accept <name>" + ChatColor.WHITE + "- Accept the Recruitment");    
        return true;
    }

}
