package me.herlex.recruteme.commands;
     
import me.herlex.recruteme.RecruteMe;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
     
public class CommandRecrute extends CommandBase
{      
    public CommandRecrute (RecruteMe main)
    {
        super(main);
    }
       
    public boolean execute(Player player, String[] args)
    {    
        Player other = Bukkit.getPlayerExact(args[0]);
        
        if(player.hasPermission("recruteme.recrute") || player.hasPermission("recruteme.*")) 
        {
            if(other == null)
            {
                player.sendMessage(ChatColor.translateAlternateColorCodes('&', main.getConfig().getString("main.chatcolor") + args[0] + " is offline! Try it again later!"));
                return true;
            }
            if(player.getAddress().getAddress().getHostAddress().equals(other.getAddress().getAddress().getHostAddress()))
            {
                player.sendMessage(ChatColor.translateAlternateColorCodes('&', main.getConfig().getString("main.chatcolor") + "Same IP isn't allowed!"));
                return true;
            }
            if(player.getName().equalsIgnoreCase(args[0]))
            {
                player.sendMessage(ChatColor.translateAlternateColorCodes('&', main.getConfig().getString("main.chatcolor") + "Don't recrute yourself!"));
                return true;
            }
            ConfigurationSection s1 = main.getData().getConfigurationSection("recrutes");
            if(s1 == null)
                s1 = main.getData().createSection("recrutes");
        
            ConfigurationSection nameSection = s1.getConfigurationSection(player.getName());
        
            if(nameSection != null)
            {
                player.sendMessage(ChatColor.translateAlternateColorCodes('&', main.getConfig().getString("main.chatcolor") + "You already entered the Recruiter"));
                return true;
            }
            nameSection = s1.createSection(player.getName());
            nameSection.set("recruter", args[0]);
            nameSection.set("time", 0);
            nameSection.set("accept", "false");
            main.save();
            player.sendMessage(ChatColor.translateAlternateColorCodes('&', main.getConfig().getString("main.chatcolor") + "Your Recruiter: " + args[0]));
            other.sendMessage(ChatColor.translateAlternateColorCodes('&', main.getConfig().getString("main.chatcolor") + player.getName() + " selected you as Recruiter! Accept with /rm accept " + player.getName()));
        }
        else
        {
            player.sendMessage(ChatColor.RED + "You don't have the Permissions!");
        }
        return true;
    }
}
